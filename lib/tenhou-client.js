const Constants = require('./constants.js');
const GameState = require('./game-state.js');
const TenhouUtils = require('./tenhou-utils.js');

// Constants
const TenhouDotNet = Constants.TenhouDotNet;

// Standard modules
const Net = require('net');
const Events = require('events');

// 3rd party modules
const Xmldom = require('xmldom');

class TenhouClient {

    constructor() {
        this._xmlParser = new Xmldom.DOMParser();

        // Initialize constants
        this._tenhouAddress = TenhouDotNet.address;
        this._tenhouPort = TenhouDotNet.port;
        this._sendZFrequency = 5000;

        // Event emitter for notifying client controller
        this._eventEmitter = new Events.EventEmitter();

        // Set default values
        this._currentLobby = 0;
        this._taku = 0;
        this._autoConfirm = true;
        this._authenticated = false;
        this._gameState = new GameState();
    }

    /**
     * Add an event listener to listen to an event
     * @param {string} event The name of the event to listen to
     * @param {func} handler The event's handler function
     */
    registerListener(event, handler) {
        this._eventEmitter.addListener(event, handler);
    }

    /**
     * Get a copy of the current game state
     */
    getGameState() {
        return this._gameState.get();
    }

    /**
     * Sets whether the client should automatically send confirmation on things like NEXTREADY, GOK etc
     */
    setAutoConfirm(auto) {
        this._autoConfirm = auto;
    }

    /**
    * Below are methods that send OUTGOING messages to Tenhou's server
    */

    /**
     * Connect to tenhou
     */
    connect() {
        return new Promise((resolve, reject) => {
            // Create a socket
            this._client = new Net.Socket();
            this._client.setEncoding('utf8');

            // Set event handlers
            this._client.on('data', (data) => {
                // When recieve message
                let messages = data.split('\0');

                for (let i = 0; i < messages.length; i++) {
                    if (messages[i].length > 0) {
                        this._handleXML(messages[i]);
                    }
                }
            });

            // Method for keeping connection alive
            this.sendZMsg = () => {
                this._client.write('<Z />\0');
            };

            this._client.connect(this._tenhouPort, this._tenhouAddress, () => {
                this._eventEmitter.emit('onHandShake');
                resolve();
            });
        });
    }

    /**
     * Login to the client
     * @param {string} name Player ID or 'NoName'
     * @param {string} sx Sex can be 'M' or 'F'
     */
    login(name = 'NoName', sx = 'M') {
        this._gender = sx;
        this._client.write('<HELO name="' + name + '" tid="f0" sx="' + sx + '" />\0');

        return new Promise((resolve, reject) => {
            this._resolveLogin = resolve;
        });
    }

    /**
     * Send auth token for logging in
     * @param {string} val 
     */
    auth(val) {
        this._client.write('<AUTH val="' + val + '"/>\0');

        return new Promise((resolve, reject) => {
            this._resolveAuth = resolve;
        });
    }

    /**
     * Join a lobby
     * @param {number} lobby Lobby ID
     */
    joinLobby(lobby) {
        this._client.write('<CHAT text="%2Flobby%20' + lobby + '" />\0');

        return new Promise((resolve, reject) => {
            this._resolveJoinJobby = resolve;
        });
    }

    /**
     * Join a table given a table ID
     * IPPAN: 1
     * JYOU: 129
     * TOKUJ: 33
     * HOUOU: 161
     * @param {number} taku Taku ID, or 0 to stay connected
     */
    switchTaku(taku) {
        if (this._currentLobby !== 0) {
            // Private room
            this._client.write('<PXR V="-1" />\0');
        } else {
            // When first login
            if (this._username === 'NoName') {
                this._client.write('<PXR V="0" />\0');
            } else {
                this._client.write('<PXR V="' + taku + '" />\0');
            }
        }
        this._currentTaku = taku;
    }

    /**
     * Request lobby status
     */
    fetchLobbyInfo() {
        if (this._currentLobby !== 0) {
            // Private room
            this._client.write('<PXR v="-1" />\0');
        } else {
            this._client.write('<PXR V="' + this._currentTaku + '" />\0');
        }
    }

    /**
     * Send a message to chat
     * @param {string} text Message to send
     */
    say(text) {
        this._client.write('<CHAT text="' + text + '" />\0');
    }

    /**
     * Queue for a game
     * Game IDs:
     * Tonpuu, no reds: 7
     * Tonpuu, no reds, no open tanyao: 3
     * Tonpuu: 1
     * Tonpuu, fast: 65
     * Tonnan, no reds: 15
     * Tonnan, no reds, no open tanyao: 1
     * Tonnan: 9
     * Tonnan, fast: 73
     * @param {number} gameTypeId 
     */
    queue(gameTypeId) {
        this._client.write('<JOIN t="' + this._currentLobby + ',' + gameTypeId + '" />\0');
    }

    /**
     * Cancel all current game queues
     */
    cancelQueue() {
        this._client.write('<JOIN />\0');
    }

    /**
     * Send OK message to signal player is ready for game
     */
    gok() {
        this._client.write('<GOK />\0');
    }

    /**
     * Send OK message to signal player is ready for next round
     */
    nextReady() {
        this._client.write('<NEXTREADY />\0');
    }

    /**
     * Disconnect from a game
     */
    bye() {
        clearInterval(this._keepAlive);
        this._client.write('<Bye />\0');
        this._client.destroy();
    }

    /**
     * Pass on an action such as chii or riichi
     */
    pass() {
        this._client.write('<N />\0');
    }

    /**
     * Discard a tile
     * @param {number} tileId 
     */
    discard(tileId) {
        this._client.write('<D p="' + tileId + '" />\0');
    }

    /**
     * Call pon
     * @param {number} tileId1 ID of tile 1 from hand to form the pon
     * @param {number} tileId2 ID of tile 2 from hand to form the pon
     */
    pon(tileId1, tileId2) {
        this._client.write('<N type="1" hai0="' + tileId1 + '" hai1="' + tileId2 + '" />\0');
    }

    /**
     * Declare open kan
     */
    minkan() {
        this._client.write('<N type="2" />\0');
    }

    /**
     * Declare chii
     * @param {number} tileId1 ID of tile 1 from hand to form the chii
     * @param {number} tileId2 ID of tile 2 from hand to form the chii
     */
    chii(tileId1, tileId2) {
        this._client.write('<N type="3" hai0="' + tileId1 + '" hai1="' + tileId2 + '" />\0');
    }

    /**
     * Declare a consealed kan
     * @param {tileId} tileId ID of the tile to form the kan
     */
    ankan(tileId) {
        this._client.write('<N type="4" hai="' + tileId + '" />\0');
    }

    /**
     * Declare chakan
     * @param {number} tileId ID of the tile to form the chakan
     */
    chakan(tileId) {
        this._client.write('<N type="5" hai="' + tileId + '" />\0');
    }

    /**
     * Declare ron
     */
    ron() {
        this._client.write('<N type="6" />\0');
    }

    /**
     * Declare tsumo
     */
    tsumo() {
        this._client.write('<N type="7" />\0');
    }

    /**
     * Declare ryuukyuku (Kyuushu-kyuuha)
     */
    ryuukyoku() {
        this._client.write('<N type="9" />\0');
    }

    /**
     * No use for now since 3-ma is not supported
     */
    nuku() {
        this._client.write('<N type="10" />\0');
    }

    /**
     * Declare riichi
     * Note: the tile ID actually is the tile with the smallest tile ID that can be discarded for tenpai
     * This tile is not actually the actual tile to be discarded
     * @param {number} tileId Tile ID of the riichi valid discard
     */
    riichi(tileId) {
        this._client.write('<REACH hai="' + tileId + '" />\0');
    }

    /**
    * Handle incoming messages from Tenhou's server
    */
    _handleXML(msg) {
        const el = this._xmlParser.parseFromString(msg).documentElement;

        if (el.tagName === 'HELO') {
            // Login successful
            const auth = el.getAttribute('auth');
            const username = el.getAttribute('uname');

            this._username = TenhouUtils.decodeString(username);
            this.auth(TenhouUtils.decodeAuth(auth));
            setTimeout(() => {
                this.switchTaku(0);
                this._eventEmitter.emit('onLogin');
                this._resolveLogin();
            }, 700);
        } else if (el.tagName === 'LN') {
            // LN contains information such as ranks, number of players etc
            this.fetchLobbyInfo();
            if (!this._authenticated) {
                this._keepAlive = setInterval(this.sendZMsg, this._sendZFrequency);
                this._authenticated = true;
                this._eventEmitter.emit('onAuthenticated');
                this._resolveAuth();
            }
            this._eventEmitter.emit('onLobbyBroadcast');
        } else if (el.tagName === 'CHAT') {
            // Chat or confirm joined lobby
            const newLobby = el.getAttribute('lobby');

            if (newLobby !== '') {
                // Handle lobby change event
                this._eventEmitter.emit('onLobbyJoined', parseInt(newLobby));
                this._currentLobby = parseInt(newLobby);
                this._resolveJoinJobby(newLobby);
            }
            const uname = TenhouUtils.decodeString(el.getAttribute('uname'));
            const text = TenhouUtils.decodeString(el.getAttribute('text'));

            if (uname !== '' || text !== '') {
                this._eventEmitter.emit('onPlayerChat', uname, text);
            }
        } else if (el.tagName === 'GO') {
            // Found a game
            const type = parseInt(el.getAttribute('type'));
            const lobby = parseInt(el.getAttribute('lobby'));
            const gpid = el.getAttribute('gpid');
            const gameData = { type, lobby, gpid };

            this._gameState.mergeState(gameData);
            this._eventEmitter.emit('onGameFound', gameData);
            if (this._autoConfirm) {
                this.gok();
                this.nextReady();
            }
        } else if (el.tagName === 'UN') {
            // UN contains information about the players in current game
            // Also triggered by rejoin
            if (el.getAttribute('n0') === '') {
                // Someone reconnected
                let who = '';

                if (el.getAttribute('n1') !== '') {
                    who = 1;
                } else if (el.getAttribute('n2') !== '') {
                    who = 2;
                } else if (el.getAttribute('n3') !== '') {
                    who = 3;
                }
                this._gameState.mergePlayerState({ connected: true }, who);
                this._eventEmitter.emit('onPlayerReconnect', who);
            } else {
                const p2Name = TenhouUtils.decodeString(el.getAttribute('n0'));
                const p3Name = TenhouUtils.decodeString(el.getAttribute('n1'));
                const p4Name = TenhouUtils.decodeString(el.getAttribute('n2'));
                const dans = el.getAttribute('dan').split(',').map(Number);
                const ratings = el.getAttribute('rate').split(',').map(Number);
                const self = { name: this._username, dan: dans[0], rating: ratings[0] };
                const player2 = { name: p2Name, dan: dans[1], rating: ratings[1] };
                const player3 = { name: p3Name, dan: dans[2], rating: ratings[2] };
                const player4 = { name: p4Name, dan: dans[3], rating: ratings[3] };
                const gameData = { self, player2, player3, player4 };

                this._gameState.mergeState(gameData);
                this._eventEmitter.emit('onPlayersInfo', gameData);
            }
        } else if (el.tagName === 'INIT') {
            // Contains round num, riichi sticks and honba, score, dice and dora indicator
            const seed = el.getAttribute('seed').split(',').map(Number);
            const ba = seed[0];
            const honba = seed[1];
            const riichiSticks = seed[2];
            const dice1 = seed[3];
            const dice2 = seed[4];
            const dora = seed[5];
            const points = el.getAttribute('ten').split(',').map(Number);
            const hand = el.getAttribute('hai').split(',').map(Number);
            const oya = parseInt(el.getAttribute('oya'));
            const self = { points: points[0], hand };
            const player2 = { points: points[1] };
            const player3 = { points: points[2] };
            const player4 = { points: points[3] };
            const gameData = { self, player2, player3, player4, ba, honba, riichiSticks, dice1, dice2, doraList: [dora], oya };

            this._gameState.newRound();
            this._gameState.mergeState(gameData);
            this._eventEmitter.emit('onGameInit', gameData);
        } else if (el.tagName === 'TAIKYOKU') {
            const oya = parseInt(el.getAttribute('oya'));
            const log = el.getAttribute('log');
            const gameData = { oya, log };

            this._gameState.mergeState(gameData);
            this._eventEmitter.emit('onTaikyoku', gameData);
        } else if (el.tagName === 'AGARI') {
            const honba = parseInt(el.getAttribute('ba').split(',')[0]);
            const riichiSticks = parseInt(el.getAttribute('ba').split(',')[1]);
            const winningHand = el.getAttribute('hai').split(',').map(Number);
            const machi = parseInt(el.getAttribute('machi'));

            // Split ten into fu / points / mangan level
            const ten = el.getAttribute('ten').split(',').map(Number);
            const fu = ten[0];
            const points = ten[1];
            const manganLevel = ten[2]; // 1 = mangan, 2 = haneman... etc
            // Each yaku is represented by 2 consecutive values
            // First number is yaku id, second number is han
            const yakus = el.getAttribute('yaku').split(',').map(Number);
            const yakuList = {};

            for (let i = 0; i < yakus.length; i += 2) {
                yakuList[yakus[i]] = yakus[i + 1];
            }
            const doraHais = el.getAttribute('doraHai').split(',').map(Number);
            const doraHaiUras = el.getAttribute('doraHaiUra').split(',').map(Number);
            const who = parseInt(el.getAttribute('who'));
            const fromWho = parseInt(el.getAttribute('fromWho'));

            // Each player's point change is represented by 2 consecutive values
            // First number is original, second number difference
            const pointsChanges = el.getAttribute('sc').split(',').map(Number);
            let yakumans = [];

            if (el.getAttribute('yakuman') !== '') {
                yakumans = el.getAttribute('yakuman').split(',').map(Number);
            }
            const fuuroList = [];

            if (el.getAttribute('m') !==  '') {
                const fuuros = el.getAttribute('m').split(',').map(Number);

                for (let i = 0; i < fuuros.length; i++) {
                    fuuroList.push(TenhouUtils.decodeM(fuuros[i]));
                }
            }
            let owari = [];

            if (el.getAttribute('owari') !== '') {
                owari = el.getAttribute('owari').split(',').map(Number);
            }
            const agariData = {
                honba,
                riichiSticks,
                winningHand,
                machi,
                fu,
                points,
                manganLevel,
                yakuList,
                doraHais,
                doraHaiUras,
                who,
                fromWho,
                pointsChanges,
                yakumans,
                fuuroList,
                owari,
            };

            this._gameState.applyPointsChanges(pointsChanges[1], pointsChanges[3], pointsChanges[5], pointsChanges[7]);
            this._eventEmitter.emit('onAgari', agariData);
            if (this._autoConfirm) {
                if (el.getAttribute('owari') !== '') {
                    this.bye();
                } else {
                    this.nextReady();
                }
            }
            if (el.getAttribute('owari') !== '') {
                this._gameState.mergeState({
                    self: { points: owari[0] },
                    player2: { points: owari[2] },
                    player3: { points: owari[4] },
                    player4: { points: owari[6] },
                });
                this._eventEmitter.emit('onGameEnd');
            }
        } else if (el.tagName === 'RYUUKYOKU') {
            const honba = el.getAttribute('ba').split(',')[0];
            const riichiSticks = el.getAttribute('ba').split(',')[1];
            const hai0  = el.getAttribute('hai0');
            const hai1  = el.getAttribute('hai1');
            const hai2  = el.getAttribute('hai2');
            const hai3  = el.getAttribute('hai3');
            const pointsChanges = el.getAttribute('sc').split(',').map(Number);
            let owari = [];

            if (el.getAttribute('owari') !== '') {
                owari = el.getAttribute('owari').split(',').map(Number);
            }
            const type = el.getAttribute('type'); // Nagashi mangan: type="nm"
            const ryuukyokuData = {
                honba,
                riichiSticks,
                hai0,
                hai1,
                hai2,
                hai3,
                pointsChanges,
                owari,
                type,
            };

            this._gameState.applyPointsChanges(pointsChanges[1], pointsChanges[3], pointsChanges[5], pointsChanges[7]);
            this._eventEmitter.emit('onRyuukyoku', ryuukyokuData);
            if (this._autoConfirm) {
                if (el.getAttribute('owari') !== '') {
                    this.bye();
                } else {
                    this.nextReady();
                }
            }
            if (el.getAttribute('owari') !== '') {
                this._gameState.mergeState({
                    self: { points: owari[0] },
                    player2: { points: owari[2] },
                    player3: { points: owari[4] },
                    player4: { points: owari[6] },
                });
                this._eventEmitter.emit('onGameEnd');
            }
        } else if (el.tagName === 'DORA') {
            const doraHai = parseInt(el.getAttribute('hai'));

            this._gameState.mergeState({ doraList: [doraHai] });
            this._eventEmitter.emit('onDora', doraHai);
        } else if (el.tagName === 'BYE') {
            const who = parseInt(el.getAttribute('who'));

            this._gameState.mergePlayerState({ connected: false }, who);
            this._eventEmitter.emit('onPlayerDisconnect', who);
        } else if (el.tagName === 'REACH') {
            const step = el.getAttribute('step');
            const who = parseInt(el.getAttribute('who'));

            if (step === '1') {
                this._gameState.mergePlayerState({ riichi: true }, who);
                this._eventEmitter.emit('onRiichiStepOne', who);
            } else {
                const points = el.getAttribute('ten').split(',').map(Number);
                const self = { points: points[0] };
                const player2 = { points: points[1] };
                const player3 = { points: points[2] };
                const player4 = { points: points[3] };
                const gameData = { self, player2, player3, player4 };

                this._gameState.mergeState(gameData);
                this._eventEmitter.emit('onRiichiStepTwo', gameData, who);
            }
        } else if (el.tagName === 'N') {
            const who = parseInt(el.getAttribute('who'));
            const m = el.getAttribute('m');
            const nakiData = TenhouUtils.decodeM(m);

            this._gameState.mergePlayerState({ fuuroList: [nakiData] }, who);
            this._eventEmitter.emit('onNaki', nakiData, who);
        } else if (el.tagName === 'U') {
            this._gameState.drawFromWall();
            this._eventEmitter.emit('onTileDraw', 1);
        } else if (el.tagName === 'V') {
            this._gameState.drawFromWall();
            this._eventEmitter.emit('onTileDraw', 2);
        } else if (el.tagName === 'W') {
            this._gameState.drawFromWall();
            this._eventEmitter.emit('onTileDraw', 3);
        } else if (el.tagName === 'PROF') {
            // Lets ignore this for now
        } else if (el.tagName === 'REJOIN') {
            const gameTypeId = el.getAttribute('t').split(',')[1];

            this.queue(gameTypeId);
        } else if (el.tagName === 'FURITEN') {
            const isFuriten = el.getAttribute('show');

            if (isFuriten === '1') {
                this._gameState.mergePlayerState({ furiten: true }, 0);
            } else {
                this._gameState.mergePlayerState({ furiten: false }, 0);
            }
        } else if (msg.startsWith('<T')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDrawn = tileObject.tile;
            const actions = el.getAttribute('t') === '' ? null : parseInt(el.getAttribute('t'));

            // Cache the tile drawn for self discard tedashi checking

            this._tileDrawn = tileDrawn;
            this._gameState.drawFromWall();
            this._gameState.mergePlayerState({ hand: [tileDrawn] }, 0);
            this._eventEmitter.emit('onTileDraw', 0, tileDrawn, actions);
        } else if (msg.startsWith('<D')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDiscarded = tileObject.tile;
            const actions = null;

            this._gameState.discard(tileDiscarded, 0, tileDiscarded !== this._tileDrawn);
            this._eventEmitter.emit('onTileDiscard', 0, tileDiscarded, actions);
        } else if (msg.startsWith('<E')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDiscarded = tileObject.tile;
            const actions = el.getAttribute('t') === '' ? null : parseInt(el.getAttribute('t'));

            this._gameState.discard(tileDiscarded, 1, true);
            this._eventEmitter.emit('onTileDiscard', 1, tileDiscarded, actions, true);
        } else if (msg.startsWith('<F')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDiscarded = tileObject.tile;
            const actions = el.getAttribute('t') === '' ? null : parseInt(el.getAttribute('t'));

            this._gameState.discard(tileDiscarded, 2, true);
            this._eventEmitter.emit('onTileDiscard', 2, tileDiscarded, actions, true);
        } else if (msg.startsWith('<G')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDiscarded = tileObject.tile;
            const actions = el.getAttribute('t') === '' ? null : parseInt(el.getAttribute('t'));

            this._gameState.discard(tileDiscarded, 3, true);
            this._eventEmitter.emit('onTileDiscard', 3, tileDiscarded, actions, true);
        } else if (msg.startsWith('<e')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDiscarded = tileObject.tile;
            const actions = el.getAttribute('t') === '' ? null : parseInt(el.getAttribute('t'));

            this._gameState.discard(tileDiscarded, 1, false);
            this._eventEmitter.emit('onTileDiscard', 1, tileDiscarded, actions, false);
        } else if (msg.startsWith('<f')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDiscarded = tileObject.tile;
            const actions = el.getAttribute('t') === '' ? null : parseInt(el.getAttribute('t'));

            this._gameState.discard(tileDiscarded, 2, false);
            this._eventEmitter.emit('onTileDiscard', 2, tileDiscarded, actions, false);
        } else if (msg.startsWith('<g')) {
            const tileObject = TenhouUtils.parseTileAction(el.tagName);
            const tileDiscarded = tileObject.tile;
            const actions = el.getAttribute('t') === '' ? null : parseInt(el.getAttribute('t'));

            this._gameState.discard(tileDiscarded, 3, false);
            this._eventEmitter.emit('onTileDiscard', 3, tileDiscarded, actions, false);
        }
    }
}

exports = module.exports = TenhouClient;
exports.Constants = Constants;
exports.GameState = GameState;

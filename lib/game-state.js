class GameState {
    constructor() {
        this._gameState = Object.seal({
            self: this._createSelf(),
            player2: this._createPlayer(),
            player3: this._createPlayer(),
            player4: this._createPlayer(),
            type: 0,
            lobby: 0,
            gpid: '',
            log: '',
            ba: 0,
            honba: 0,
            riichiSticks: 0,
            dice1: 0,
            dice2: 0,
            oya: 0,
            wall: 70,
            doraList: [],
            unseenTileList: this._createTiles(),
            globalDiscardList: [],
        });
    }

    _createSelf() {
        return Object.seal({
            name: '',
            dan: 0,
            rating: 0.00,
            connected: true,
            points: 0,
            riichi: false,
            riichiTile: null,
            furiten: false,
            hand: [],
            fuuroList: [],
            discardList: [],
            tedashiList: [],
        });
    }

    _createPlayer() {
        return Object.seal({
            name: '',
            dan: 0,
            rating: 0.00,
            connected: true,
            points: 0,
            riichi: false,
            riichiTile: null,
            fuuroList: [],
            discardList: [],
            tedashiList: [],
        });
    }

    _createTiles() {
        const arr = [];

        for (let i = 0; i < 136; i++) {
            arr.push(i);
        }

        return arr;
    }

    _deepMerge(a, b) {
        for (let key of Object.keys(b)) {
            if (typeof a[key] === typeof b[key]) {
                if (Array.isArray(b[key])) {
                    a[key] = a[key].concat(b[key]);
                } else if (typeof b[key] === 'object') {
                    this._deepMerge(a[key], b[key]);
                } else {
                    a[key] = b[key];
                }
            } else {
                console.log('Unexpected key when updating gameState: ' + key);
                console.log('a = ' + typeof a + ' b = ' + typeof b);
                console.log('a[key] = ' + a[key] + ' b[key] = ' + b[key]);
            }
        }
    }

    _getPlayer(who) {
        switch (who) {
            case 0:
                return this._gameState.self;
            case 1:
                return this._gameState.player2;
            case 2:
                return this._gameState.player3;
            case 3:
                return this._gameState.player4;
        }
    }

    _clone(o) {
        let out, v, key;

        out = Array.isArray(o) ? [] : {};
        for (key in o) {
            v = o[key];
            out[key] = (typeof v === 'object') ? this._clone(v) : v;
        }

        return out;
    }

    _updateUnseenTileList() {
        const state = this._gameState;
        let seenTileList = [].concat(
            state.globalDiscardList,
            state.self.hand,
            state.self.fuuroList,
            state.player2.fuuroList,
            state.player3.fuuroList,
            state.player4.fuuroList,
            state.doraList
        );

        state.unseenTileList = state.unseenTileList.filter((tile) => {
            return !seenTileList.includes(tile);
        });
    }

    _updateHand() {
        const self = this._gameState.self;

        self.hand = self.hand.filter((tile) => {
            for (let i = 0; i < self.fuuroList.length; i++) {
                let fuuroTiles = self.fuuroList[i].tiles;

                for (let j = 0; j < fuuroTiles.length; j++) {
                    if (fuuroTiles[j] === tile) {
                        return false;
                    }
                }
            }

            return true;
        });
    }

    newRound() {
        this._gameState.doraList = [];
        this._gameState.globalDiscardList = [];
        this._gameState.wall = 70;
        this._gameState.unseenTileList = this._createTiles();
        this._gameState.self.riichi = false;
        this._gameState.self.riichiTile = null;
        this._gameState.self.furiten = false;
        this._gameState.self.hand = [];
        this._gameState.self.fuuroList = [];
        this._gameState.self.discardList = [];
        this._gameState.self.tedashiList = [];
        this._gameState.player2.riichi = false;
        this._gameState.player2.riichiTile = null;
        this._gameState.player2.fuuroList = [];
        this._gameState.player2.discardList = [];
        this._gameState.player2.tedashiList = [];
        this._gameState.player3.riichi = false;
        this._gameState.player3.riichiTile = null;
        this._gameState.player3.fuuroList = [];
        this._gameState.player3.discardList = [];
        this._gameState.player3.tedashiList = [];
        this._gameState.player4.riichi = false;
        this._gameState.player4.riichiTile = null;
        this._gameState.player4.fuuroList = [];
        this._gameState.player4.discardList = [];
        this._gameState.player4.tedashiList = [];
    }

    drawFromWall() {
        this.mergeState({ wall: this._gameState.wall - 1 });
    }

    discard(tile, who, isTedashi) {
        const player = this._getPlayer(who);

        if (player.riichi && player.riichiTile === null) {
            player.riichiTile = tile;
        }
        if (who === 0) {
            this._gameState.self.hand = this._gameState.self.hand.filter((t) => {
                return tile !== t;
            });
        }
        this.mergePlayerState({ discardList: [tile] }, who);
        if (isTedashi) {
            this.mergePlayerState({ tedashiList: [tile] }, who);
        }
        this._gameState.globalDiscardList.push(tile);
    }

    mergeState(gameData) {
        this._deepMerge(this._gameState, gameData);
        this._updateUnseenTileList();
    }

    mergePlayerState(playerData, who) {
        if (who === 0) {
            this.mergeState({ self: playerData });
            this._updateHand();
        } else if (who === 1) {
            this.mergeState({ player2: playerData });
        } else if (who === 2) {
            this.mergeState({ player3: playerData });
        } else if (who === 3) {
            this.mergeState({ player4: playerData });
        }
        this._updateUnseenTileList();
    }

    applyPointsChanges(selfDelta, player2Delta, player3Delta, player4Delta) {
        this._gameState.self.points += selfDelta;
        this._gameState.player2.points += player2Delta;
        this._gameState.player3.points += player3Delta;
        this._gameState.player4.points += player4Delta;
    }

    get() {
        return this._clone(this._gameState);
    }
}

module.exports = GameState;

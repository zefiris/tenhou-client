module.exports.TenhouDotNet = Object.freeze({
    address: '133.242.10.78',
    port: '10080',
});

module.exports.GameTypeIds = Object.freeze({
    E_CAN_OPEN_TANYAO: 7,
    E_CANT_OPEN_TANYAO: 3,
    E_AKADORA: 1,
    E_AKADORA_FAST: 65,
    ES_CAN_OPEN_TANYAO: 15,
    ES_CANT_OPEN_TANYAO: 11,
    ES_AKADORA: 9,
    ES_AKADORA_FAST: 73,
});

module.exports.TakuIds = Object.freeze({
    IPPAN_TAKU: 1,
    JYOU_TAKU: 129,
    TOKUJYOU_TAKU: 33,
    HOUOU_TAKU: 161,
});

module.exports.MeldTypes = Object.freeze({
    CHI: 1,
    PON: 2,
    DAIMINKAN: 3,
    CHAKAN: 4,
    ANKAN: 5,
});

module.exports.PlayerActions = Object.freeze({
    PON: 1,
    CHI: 4,
    RON: 8,
    TSUMO: 16,
    RIICHI: 32,
});

module.exports.Events = Object.freeze({
    HAND_SHAKE: 'onHandShake',
    AUTHENTICATED: 'onAuthenticated',
    LOGIN: 'onLogin',
    LOBBY_BROADCAST: 'onLobbyBroadcast',
    LOBBY_JOINED: 'onLobbyJoined',
    PLAYER_CHAT: 'onPlayerChat',
    GAME_FOUND: 'onGameFound',
    PLAYERS_INFO: 'onPlayersInfo',
    PLAYER_RECONNECT: 'onPlayerReconnect',
    GAME_INIT: 'onGameInit',
    TAIKYOKU: 'onTaikyoku',
    AGARI: 'onAgari',
    RYUUKYOKU: 'onRyuukyoku',
    GAME_END: 'onGameEnd',
    DORA: 'onDora',
    PLAYER_DISCONNECT: 'onPlayerDisconnect',
    RIICHI_STEP_ONE: 'onRiichiStepOne',
    RIICHI_STEP_TWO: 'onRiichiStepTwo',
    NAKI: 'onNaki',
    TILE_DRAW: 'onTileDraw',
    TILE_DISCARD: 'onTileDiscard',
});

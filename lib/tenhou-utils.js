const querystring = require('querystring');
const Constants = require('./constants.js');
const MeldTypes = Constants.MeldTypes;

module.exports.decodeAuth = (auth) => {
    const translationTable = [
        63006, 9570, 49216, 45888,
        9822, 23121, 59830, 51114,
        54831, 4189, 580, 5203,
        42174, 59972, 55457, 59009,
        59347, 64456, 8673, 52710,
        49975, 2006, 62677, 3463,
        17754, 5357,
    ];

    const part1 = auth.split('-')[0];
    const part2 = auth.split('-')[1];
    const tableIndex = parseInt('2' + part1.substring(2, 8)) % (12 - parseInt(part1.substring(7, 8))) * 2;
    const a = translationTable[tableIndex] ^ parseInt(part2.substring(0, 4), 16);
    const b = translationTable[tableIndex + 1] ^ parseInt(part2.substring(4, 8), 16);
    const postfix = a.toString(16) + b.toString(16);
  	const result = part1 + '-' + postfix;

    return result;
};

module.exports.parseTileAction = (str) => {
    const source = parseInt(str.substring(0, 1));
    const tile = parseInt(str.substring(1));


    return { source, tile };
};

module.exports.decodeM = (val) => {
    const fromWho = val & 0x3;
    let type;

    if (val & 0x4) {
        // CHI
        const t0 = (val >> 3) & 0x3;
        const t1 = (val >> 5) & 0x3;
        const t2 = (val >> 7) & 0x3;
        const baseAndCalled = val >> 10;
        let base = Math.floor(baseAndCalled / 3);

        base = Math.floor(base / 7) * 9 + base % 7;
        const tile0 = t0 + 4 * (base + 0);
        const tile1 = t1 + 4 * (base + 1);
        const tile2 = t2 + 4 * (base + 2);

        type = MeldTypes.CHI;

        return { tiles: [tile0, tile1, tile2], type, fromWho };
    } else if (val & 0x18) {
        const t4 = (val >> 5) & 0x3;
        const t0 = [1, 0, 0, 0][t4];
        const t1 = [2, 2, 1, 1][t4];
        const t2 = [3, 3, 3, 2][t4];
        const baseAndCalled = val >> 9;
        let base = Math.floor(baseAndCalled / 3);

        if (val & 0x8) {
            const tile0 = t0 + 4 * base;
            const tile1 = t1 + 4 * base;
            const tile2 = t2 + 4 * base;

            type = MeldTypes.PON;

            return { tiles: [tile0, tile1, tile2], type, fromWho };
        } else {
            const tile0 = t0 + 4 * base;
            const tile1 = t1 + 4 * base;
            const tile2 = t2 + 4 * base;
            const tile3 = t4 + 4 * base;

            type = MeldTypes.CHAKAN;

            return { tiles: [tile0, tile1, tile2, tile3], type, fromWho };
        }
    } else if (val & 0x20) {
        // nuki (sanma exclusive)
    } else {
        const baseAndCalled = val >> 8;
        const base = Math.floor(baseAndCalled / 4);

        type = MeldTypes.DAIMINKAN;
        if (fromWho === 0) {
            type = MeldTypes.ANKAN;
        }
        const tile0 = 4 * base;
        const tile1 = 4 * base + 1;
        const tile2 = 4 * base + 2;
        const tile3 = 4 * base + 3;


        return { tiles: [tile0, tile1, tile2, tile3], type, fromWho };
    }
};

module.exports.decodeString = (str) => {
    return querystring.unescape(str);
};

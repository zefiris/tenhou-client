const TenhouClient = require('../lib/tenhou-client');
const minimist = require('minimist');

let args = minimist(process.argv.slice(2), {
    string: ['lobby', 'user-id'],
    default: { 'game-type': 1, lobby: '0', 'user-id': 'NoName' },
    alias: { g: 'game-type', l: 'lobby', u: 'user-id' },
    '--': true,
    unknown: () => {
        console.log('Options: --game-type (-g) <game type> eg: 1, --lobby (-l) <lobby> eg: 1234, --user-id (-u) <user id> eg: NoName');
        process.exit(0);
    },
});

let client = new TenhouClient();

/**
* Below are methods that handle tenhou-client events
*/
const onHandShake = () => {
    console.log('Connected...');
};

const onAuthenticated = () => {
    console.log('Authenticated...');
};

const onLogin = () => {
    console.log('Successfully logged in...');
};

const onLobbyBroadcast = () => {
    console.log('Lobby message received');
};

const onLobbyJoined = (lobby) => {
    console.log('Joined lobby ' + lobby);
};

const onPlayerChat = (who, text) => {
    console.log(who + ' says: ' + text);
};

const onGameFound = (gameData) => {
    console.log('Game found...');
    console.log(gameData);
};

const onPlayersInfo = (gameData) => {
    console.log('Players info...');
    console.log(gameData);
};

const onPlayerReconnect = (who) => {
    console.log(who + ' reconnected...');
};

const onGameInit = (gameData) => {
    console.log('Game Init...');
    console.log(gameData);
    console.log(JSON.stringify(client.getGameState()));
};

const onTaikyoku = (gameData) => {
    console.log('Taikyoku...');
    console.log(gameData);
};

const onAgari = (agariData) => {
    console.log('Agari!');
    console.log(agariData);
    console.log(JSON.stringify(client.getGameState()));
};

const onRyuukyoku = (ryuukyokuData) => {
    console.log('Ryuukyoku...');
    console.log(ryuukyokuData);
    console.log(JSON.stringify(client.getGameState()));
};

const onGameEnd = () => {
    console.log('Game ended');
    console.log(JSON.stringify(client.getGameState()));
    process.exit();
};

const onDora = (doraHai) => {
    console.log('New dora: ' + doraHai);
};

const onPlayerDisconnect = (who) => {
    console.log(who + ' disconnected...');
};

const onRiichiStepOne = (who) => {
    console.log(who + ' riiched');
};

const onRiichiStepTwo = (gameData, who) => {
    console.log(gameData);
};

const onNaki = (nakiData, who) => {
    console.log(who + ' called ', nakiData);
};

const onTileDraw = (who, tile, actions) => {
    if (who === 0) {
        console.log('I drew a ' + tile);
        if (actions) {
            console.log('I have actions ' + actions);
            client.pass();
        }
        client.discard(tile);
    } else {
        console.log(who + ' drew a tile');
    }
};

const onTileDiscard = (who, tile, actions, isTedashi) => {
    if (actions) {
        console.log('I have actions ' + actions);
        client.pass();
    }
    if (isTedashi) {
        console.log(who + ' discarded ' + tile + ' (tedashi)');
    } else {
        console.log(who + ' discarded ' + tile + ' (tsumogiri)');
    }
};

client.registerListener('onHandShake', onHandShake);
client.registerListener('onAuthenticated', onAuthenticated);
client.registerListener('onLogin', onLogin);
client.registerListener('onLobbyBroadcast', onLobbyBroadcast);
client.registerListener('onLobbyJoined', onLobbyJoined);
client.registerListener('onPlayerChat', onPlayerChat);
client.registerListener('onGameFound', onGameFound);
client.registerListener('onPlayersInfo', onPlayersInfo);
client.registerListener('onPlayerReconnect', onPlayerReconnect);
client.registerListener('onGameInit', onGameInit);
client.registerListener('onTaikyoku', onTaikyoku);
client.registerListener('onAgari', onAgari);
client.registerListener('onRyuukyoku', onRyuukyoku);
client.registerListener('onGameEnd', onGameEnd);
client.registerListener('onDora', onDora);
client.registerListener('onPlayerDisconnect', onPlayerDisconnect);
client.registerListener('onRiichiStepOne', onRiichiStepOne);
client.registerListener('onRiichiStepTwo', onRiichiStepTwo);
client.registerListener('onNaki', onNaki);
client.registerListener('onTileDraw', onTileDraw);
client.registerListener('onTileDiscard', onTileDiscard);

/**
 * Start the client!
 */
client.connect()
    .then(() => {
        return client.login(args.u);
    })
    .then(() => {
        return client.switchTaku(1);
    })
    .then(() => {
        return client.joinLobby(args.l);
    })
    .then(() => {
        client.queue(args.g);
        console.log('Queuing: ' + args.g);
    });

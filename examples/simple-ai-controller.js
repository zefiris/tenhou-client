const TenhouClient = require('../lib/tenhou-client');
const ShantenUtils = require('./utils/shanten-utils.js');
const Constants = require('../lib/constants.js');
const PlayerActions = Constants.PlayerActions;

const minimist = require('minimist');

let args = minimist(process.argv.slice(2), {
    string: ['lobby', 'user-id'],
    default: { 'game-type': 1, lobby: '0', 'user-id': 'NoName' },
    alias: { g: 'game-type', l: 'lobby', u: 'user-id' },
    '--': true,
    unknown: () => {
        console.log('Options: --game-type (-g) <game type> eg: 1, --lobby (-l) <lobby> eg: 1234, --user-id (-u) <user id> eg: NoName');
        process.exit(0);
    },
});

let client = new TenhouClient();

console.log('Connecting...');

client.connect()
    .then(() => {
        console.log('Authenticated...');

        return client.login(args.u);
    })
    .then(() => {
        console.log('Successfully logged in...');

        return client.switchTaku(1);
    })
    .then(() => {
        return client.joinLobby(args.l);
    })
    .then(() => {
        client.queue(args.g);
        console.log('Queuing: ' + args.g);
    });

const onGameEnd = () => {
    console.log('Game ended');
    console.log(JSON.stringify(client.getGameState()));
    process.exit();
};

const onTileDraw = (who, tile, actions) => {
    let log = 'Draw:' + ShantenUtils.convertHandToTenhouNotation([tile]) + ' ';

    if (who === 0) {
        let riichi = false;

        if (actions) {
            if (actions & PlayerActions.TSUMO) {
                log += 'Tsumo ';
                client.tsumo();
            } else if (actions & PlayerActions.RIICHI) {
                riichi = true;
            } else {
                if (actions >= 64) {
                    console.log('Unknown action ' + actions);
                }
                client.pass();
            }
        }

        // Get the Gamestate and calculate the discard
        const gameState = client.getGameState();

        log += 'Hand:' + ShantenUtils.convertHandToTenhouNotation(gameState.self.hand) + ' ';

        if (gameState.self.riichi && !riichi) {
            // If we already riichied, just tsumogiri
            log += 'Discard:' + ShantenUtils.convertHandToTenhouNotation([tile]) + ' ';
            client.discard(tile);
        } else {
            // Find the most efficient discard
            const lowestShantenDiscards = getLowestShantenDiscards(gameState.self.hand);
            let highestTileAcceptanceNumber = 0;
            let highestTileAcceptanceDiscards = [];

            for (let i = 0; i < lowestShantenDiscards.length; i++) {
                const testHand = gameState.self.hand.filter((t) => {
                    return t !== lowestShantenDiscards[i];
                });
                const tileAcceptanceNumber = getTileAcceptanceCount(testHand, gameState.unseenTileList);

                if (tileAcceptanceNumber > highestTileAcceptanceNumber) {
                    highestTileAcceptanceNumber = tileAcceptanceNumber;
                    highestTileAcceptanceDiscards = [lowestShantenDiscards[i]];
                } else if (tileAcceptanceNumber === highestTileAcceptanceNumber) {
                    highestTileAcceptanceDiscards.push(lowestShantenDiscards[i]);
                }
            }
            const randomIndex = Math.floor(Math.random() * highestTileAcceptanceDiscards.length);
            const tileToDiscard = highestTileAcceptanceDiscards[randomIndex];

            if (riichi && ShantenUtils.getNormalShanten(gameState.self.hand) === 0) {
                log += 'Riichi ';
                client.riichi(lowestShantenDiscards[0]);
            }
            log += 'Discard:' + ShantenUtils.convertHandToTenhouNotation([tileToDiscard]) + ' ';
            client.discard(tileToDiscard);
        }
        console.log(log);
    }
};

const onTileDiscard = (who, tile, actions, isTedashi) => {
    if (actions) {
        if (actions & PlayerActions.RON) {
            console.log('Ron ');
            client.ron();
        } else {
            client.pass();
        }
    }
};

client.registerListener('onGameEnd', onGameEnd);
client.registerListener('onTileDraw', onTileDraw);
client.registerListener('onTileDiscard', onTileDiscard);

// Expects a 14 tiles hand
const getLowestShantenDiscards = (hand) => {
    // Evaluate shanten change for each possible discard
    let lowestShantenNumber = 9;
    let lowestShantenDiscards = [];

    for (let i = 0; i < hand.length; i++) {
        const tileToEvaluate = hand[i];
        const handToEvaluate = hand.slice();

        handToEvaluate.splice(i, 1);
        const shantenNumber = ShantenUtils.getNormalShanten(handToEvaluate);

        if (shantenNumber < lowestShantenNumber) {
            lowestShantenNumber = shantenNumber;
            lowestShantenDiscards = [tileToEvaluate];
        } else if (shantenNumber === lowestShantenNumber) {
            lowestShantenDiscards.push(tileToEvaluate);
        }
    }

    return lowestShantenDiscards;
};

// Given a 13 tiles hand, and an unseenTileList, return the tile acceptance number (useful tiles)
const getTileAcceptanceCount = (hand, unseenTiles) => {
    let usefulUnseenTiles = 0;
    const currentShanten = ShantenUtils.getNormalShanten(hand);

    for (let i = 0; i < unseenTiles.length; i++) {
        const testHand = hand.concat(unseenTiles[i]);
        const shantenNumber = ShantenUtils.getNormalShanten(testHand);

        if (shantenNumber < currentShanten) {
            usefulUnseenTiles++;
        }
    }

    return usefulUnseenTiles;
};

/**
 * Javascript implementation of the Shanten Algorithm by cmj33
*/

let mentu, toitu, kouho, temp, syantenNormal;
let fuurosuu = 0;
let tehai = [];

module.exports.getNormalShanten = (hand) => {
    tehai = _convertHandToVector(hand);
    fuurosuu = 4 - Math.floor((hand.length - 1) / 3);

    return _normalSyanten();
};

// Outputs Tenhou Notation eg: 145679m22234p7s2z
module.exports.convertHandToTenhouNotation = (hand) => {
    // Make a sorted copy of the hand
    const sortedHand = hand.concat();

    sortedHand.sort((a, b) => {
        return (a - b);
    });

    let m = '',
        p = '',
        s = '',
        z = '';

    for (let i = 0; i < sortedHand.length; i++) {
        if (sortedHand[i] < 36) {
            m += Math.floor(sortedHand[i] / 4) + 1;
        }
        if (sortedHand[i] >= 36 && sortedHand[i] < 72) {
            p += Math.floor((sortedHand[i] - 36) / 4) + 1;
        }
        if (sortedHand[i] >= 72 && sortedHand[i] < 108) {
            s += Math.floor((sortedHand[i] - 72) / 4) + 1;
        }
        if (sortedHand[i] >= 108) {
            z += Math.floor((sortedHand[i] - 108) / 4) + 1;
        }
    }
    if (m !== '') {
        m += 'm';
    }
    if (p !== '') {
        p += 'p';
    }
    if (s !== '') {
        s += 's';
    }
    if (z !== '') {
        z += 'z';
    }

    return m + p + s + z;
};

// Expected input is [0, 12, 60, 67, 68 ...]
// Length should be 3n + 1 where n = [0..4]
const _convertHandToVector = (hand) => {
    // m, p, s
    // 1z ~ 4z = winds
    // 5z = haku, 6z = hatsu, 7z = zhun
    // 0 ~ 135;
    let vector = [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,
    ];

    for (let i = 0; i < hand.length; i++) {
        vector[Math.floor(hand[i] / 4)] += 1;
    }
    vector.splice(0, 0, 0);
    vector.splice(10, 0, 0);
    vector.splice(20, 0, 0);
    vector.splice(30, 0, 0);
    vector.push(-1);

    return vector;
};

// Normal hand
const _normalSyanten = () => {
    // Initialize
    mentu = 0;
    toitu = 0;
    kouho = 0;
    temp = 0;
    syantenNormal = 8;
    for (let i = 1; i < 38; i++) {
        // Pair
        if (2 <= tehai[i]) {
            toitu++;
            tehai[i] -= 2;
            _mentuCut(1);
            tehai[i] += 2;
            toitu--;
        }
    }

    // Fuuro
    if (fuurosuu === 0) {
        _mentuCut(1);
    }

    // Dead wall
    let ji = 0;

    return syantenNormal - fuurosuu * 2 + ji;
};

const _mentuCut = (i) => {
    for (; !tehai[i]; i++) {
        ;
    }
    if (i >= 38) {
        _taatuCut(1);

        return;
    }
    if (tehai[i] >= 3) {
        mentu++;
        tehai[i] -= 3;
        _mentuCut(i);
        tehai[i] += 3;
        mentu--;
    }
    if (tehai[i + 1] && tehai[i + 2] && i < 30) {
        mentu++;
        tehai[i]--, tehai[i + 1]--, tehai[i + 2]--;
        _mentuCut(i);
        tehai[i]++, tehai[i + 1]++, tehai[i + 2]++;
        mentu--;
    }
    _mentuCut(i + 1);
};

const _taatuCut = (i) => {
    for (; !tehai[i]; i++) {
        ;
    }
    if (i >= 38)    {
        temp = 8 - mentu * 2 - kouho - toitu;
        if (temp < syantenNormal) {
            syantenNormal = temp;
        }

        return;
    }
    if (mentu + kouho < 4) {
        if (tehai[i] === 2) {
            kouho++;
            tehai[i] -= 2;
            _taatuCut(i);
            tehai[i] += 2;
            kouho--;
        }
        if (tehai[i + 1] && i < 30) {
            kouho++;
            tehai[i]--, tehai[i + 1]--;
            _taatuCut(i);
            tehai[i]++, tehai[i + 1]++;
            kouho--;
        }
        if (tehai[i + 2] && i < 30 && i % 10 <= 8) {
            kouho++;
            tehai[i]--, tehai[i + 2]--;
            _taatuCut(i);
            tehai[i]++, tehai[i + 2]++;
            kouho--;
        }
    }
    _taatuCut(i + 1);
};
